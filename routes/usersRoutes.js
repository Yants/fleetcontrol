var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User  = require('../models/Users');
var WorkDay = require('../models/VehicleWorkDays');
var VehicleReport = require('../models/VehicleReports');

//CRUD
function get(req, res){
    var userId = req.params.id;
    User.findById(userId, function(err, user){
        if (err) return res.json(403,err);

        return res.json(user);
    });
}

function getAll(req, res){
    User.find({}).exec( function(err, users){
        if (err) return res.json(403,err);


        // finish the request
        return res.json(users);

    });
}

var VehicleWorkDays = require('../models/VehicleWorkDays');
function update(req, res){
    var user = req.body;
    if (user.password)
        user.password = (new User()).generateHash(user.password);
    delete user._id;

    user.updated_at = new Date();

    User.findByIdAndUpdate(req.params.id, {
        $set: user
    }, { new:true
    }, function(err, obj) {
        if (err) res.json(403, err);

        //now update the Vehicle Work days where user is aggregated in:
        VehicleWorkDays.findOneAndUpdate({
                'crew._id': req.params.id
            },
            {
                $set: {
                    'crew.$' : obj
                }
            }, function(err, userr){
                if (err) res.json(403,err);
                else res.json(200, "ok");
            });

    });



}

function del(req, res){
    User.findByIdAndRemove(req.params.id, function(err, obj) {
        if (err) res.json(403, err);
        res.json(200,{});
    });

}

router.post('/users', function(req, res){
    var user = new User(req.body);

   user.generateHashForOwnUser();
    user.save(function (err) {
        if (err) res.json (403, err);
     else res.send('inserted');
    })
});

router.put('/users/:id',function(req,res){
    if (req.params.id)
        update(req,res);
    else return res.send('wrong parameters');
} );


router.get('/users', function(req, res) {
    return getAll(req, res);
});

router.get('/users/:id', function(req, res) {
    if (req.params.id)
        return get(req, res);
    else return res.send('wrong parameters');

});

router.delete('/users/:id', function (req, res) {
    if (req.params.id)
        return del(req, res);
    else return res.send('wrong parameters');

})




module.exports = router;