var express = require('express');
var router = express.Router();
var VehicleWorkDays  = require('../models/VehicleWorkDays');


//CRUD
function get(req, res){
    var vehicleWorkDaysId = req.params.id;
    VehicleWorkDays.findOne({vehicle_number : vehicleWorkDaysId }).populate('packages').exec(function(err, vehicleWorkDays){
        if (err) return res.json(403,err);

        return res.json( vehicleWorkDays);
    });
}

function getAll(req, res){
    VehicleWorkDays.find({}).populate('packages').exec( function(err, vehicleWorkDays){
        if (err) return res.send(err);

        return res.json(vehicleWorkDays);

    });
}

function update(req, res){
    var vehicleWorkDays = req.body;
    delete vehicleWorkDays._id;

    vehicleWorkDays.updated_at = new Date();

    VehicleWorkDays.findOneAndUpdate({vehicle_number: req.params.id} , vehicleWorkDays, { upsert:true
    }, function(err, obj) {
        if (err) res.json(403,err);
        else res.json(200, {});
    });

}

function del(req, res){
    VehicleWorkDays.remove({vehicle_number :req.params.id}, function(err, obj) {
        if (err) res.json(err);
        res.json(200,{});
    });

}

var User = require('../models/Users');
router.post('/vehicleworkdays', function (req, res) {
    if (req.body.crew){
        var crew = req.body.crew;
        delete req.body.crew;
    }
    if (req.body.packages){
        var packages = req.body.packages;
        delete req.body.packages;
    }

    var vehicleWorkDays = new VehicleWorkDays(req.body);
    vehicleWorkDays.packages = [];
    vehicleWorkDays.crew = [];

    vehicleWorkDays.save(function (err) {
        if(err)
            return res.json(403,err);
        else{
            if (packages){
                vehicleWorkDays.packages = packages;
                vehicleWorkDays.markModified('packages');
            }
            if(crew){
                crew.forEach(function(member){
                    var userr = new User(member);
                    vehicleWorkDays.crew.push(userr);
                    delete userr._id;
                });
                vehicleWorkDays.markModified('crew');
           }
            vehicleWorkDays.save(function (err) {
                if (err)
                    return res.json(403, err);
                else
                    return res.json(200,"ok");
            })

        }

    })
});

router.get('/vehicleworkdays', function (req, res) {
    getAll(req, res);
});

router.get('/vehicleworkdays/:id', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else return get(req, res);
});

router.delete('/vehicleworkdays/:id', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else
        return del(req,res);
});

router.put('/vehicleworkdays/:id', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else
        return update(req, res);

});


module.exports = router;