var express = require('express');
var router = express.Router();
var User  = require('../models/Users');
var Package = require('../models/Packages');

router.get('/users/:id', function (req, res) {
    User.findById(req.params.id, function (err, user) {
        if (!err){
            res.render('user.ejs', {
                user : user// get the user out of session and pass to template
            });
        }
    });
});

router.get('/package/:id', function (req, res) {
    Package.findOne({package_number : req.params.id}, function (err, package) {
        if (err) res.json(403, "error fetching packages");
       else{
            res.render('package.ejs', {
                package : package// get the user out of session and pass to template
            });
        }
    });
});


router.get('/profile', function(req, res) {
    var allUsers =[];
    console.log(req.user.admin);
    if (req.user.admin){
        User.find(function (err, users){
            if (err) res.json(403, "error fetching users");
            allUsers = users;
            Package.find(function(err,packages){
                if (err) res.json(403, "error fetching packages");
                res.render('profile.ejs', {
                    allUsers : allUsers,
                    allPackages : packages,
                    user : req.user
                });
            });
            //console.log(allUsers);

        });
    }
    else{
        res.render('profile.ejs', {
            user : req.user// get the user out of session and pass to template
        });
    }});



function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}


module.exports = router;