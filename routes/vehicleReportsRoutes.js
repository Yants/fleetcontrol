var express = require('express');
var router = express.Router();
var VehicleReport  = require('../models/VehicleReports');

//CRUD
function get(req, res){
    var vehicleReportId = req.params.id;
    VehicleReport.findById(vehicleReportId, function(err, vehicleReport){
        if (err) return res.json(403,err);

        return res.json(200,vehicleReport);
    });
}

function getAll(req, res){
    VehicleReport.find({}).exec( function(err, vehicleReports){
        if (err) return res.json(403,err);


        // finish the request
        return res.json(200,vehicleReports);

    });
}


function update(req, res){
    var vehicleReport = req.body;
var v = new VehicleReport(vehicleReport);
    delete vehicleReport._id;
    VehicleReport.findOne({_id : {$ne : req.params.id}}, {}, { sort: { 'created_at' : -1 } }, function(err, report1) {
        if (report1) vehicleReport.speed = v.getDistanceFromLatLonInKm(report1.loc[0], report1.loc[1]) * 360;
        VehicleReport.findByIdAndUpdate(req.params.id, {
            $set: vehicleReport
        }, {
            new: true
        }, function (err, obj) {
            if (err) res.json(403, err);
            else res.json(200, {});
        });
    });
}

function del(req, res){

    VehicleReport.findByIdAndRemove(req.params.id, function(err, obj) {
        if (err) res.json(err);
        res.json(200,{});
    });

}

router.get('/vehiclereports', function (req, res) {
    getAll(req, res);
});

router.get('/vehiclereports/:id', function (req, res) {
    if (req.params.id)
        return get(req, res);
    else return res.json(403, 'wrong parameters');
});

router.post('/vehiclereports', function (req, res) {
    var vehicleReport = new VehicleReport(req.body);
    if (vehicleReport.loc)
        vehicleReport.markModified('loc');
    VehicleReport.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, report) {
        if (report) vehicleReport.speed = vehicleReport.getDistanceFromLatLonInKm(report.loc[0], report.loc[1]) * 360;
        vehicleReport.save(function (err) {
            if(err)
                return res.json(403,err);
            else
                return res.json(200,{});
        });
    });
});

router.put('/vehiclereports/:id', function (req, res) {
    if (req.params.id)
        return update(req, res);
    else return res.json(403, 'wrong parameters');
});

router.delete('/vehiclereports/:id', function (req, res) {
    if (req.params.id)
        return del(req, res);
    else return res.json(403, 'wrong parameters');
});

module.exports = router;