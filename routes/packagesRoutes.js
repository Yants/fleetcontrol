var express = require('express');
var router = express.Router();

var Package  = require('../models/Packages');
var VehicleWorkDay = require("../models/VehicleWorkDays");
var VehicleReport = require("../models/VehicleReports");
//CRUD
function get(req, res){
    var packageId = req.params.id;
    Package.findOne({package_number: packageId}, function(err, package){
        if (err) return res.json(403,err);

        res.json(200,package);
    });
}

function getAll(req, res){
    Package.find({}).exec( function(err, packages){
        if (err) return res.json(403,err);

        // var transformedPackages = packages.map(function(package) {
        //     return package.toJSON();
        // });

        // finish the request
        return res.json(200, packages);

    });
}


function update(req, res){
    var package = req.body;



   // if (package.dest)
    //   package.markModified('dest');
    delete package._id;
    package.updated_at = new Date();

    Package.findOneAndUpdate({package_number : req.params.id}, package, {  upsert: true
    }, function(err, obj){
        if (err) res.json(403, err);
        else res.json(200, {});
    });

}

function del(req, res){

    Package.remove({ package_number: req.params.id }, function(err) {
        if (err) res.json(err);
        res.json(200,{});
    });

}


router.post('/packages', function (req, res) {
    var package = new Package(req.body);
    if (package.dest)
        package.markModified('dest');
    package.save(function (err) {
        if(err)
            return res.json(403, err);
        else
            return res.json(200,{});
    })
});

router.get('/packages', function (req, res) {
   return getAll(req, res);
});

router.get('/packages/:id', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else return get(req, res);
});

router.delete('/packages/:id', function (req, res) {
   if (!req.params.id)
       return res.json("wrong params");
    else
        return del(req,res);
});

router.put('/packages/:id', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else
        return update(req, res);

});

router.get('/packages/:id/currentLocation', function (req, res) {
    if (!req.params.id)
        return res.json("wrong params");
    else return getCurrentLocation(req, res);
});

function getCurrentLocation(req, res) {
    Package.findOne({package_number : req.params.id}, function(err, package){
        if(!package) return res.json(403, err);
        else{
            VehicleWorkDay.findOne({ status: "atWork", packages :{ "$in" : [package._id]} }, function (err, workDay) {
                if (err)
                    return res.json(403, err);
                else{
                    VehicleReport.findOne( { vehicle_number : workDay.vehicle_number},{},{ sort: { 'created_at' : -1 } }, function (err, report) {
                        if (err) return res.json(403, err);
                        else return res.json(report.loc);
                    });
                }
            });
        }

    });
}

module.exports = router;