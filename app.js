//load all required modules

var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//declare node.js app
var app = express();

var port = process.env.PORT || 3000;

var passport = require('passport');
var flash    = require('connect-flash');
var session      = require('express-session');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

require('./config/passport')(passport); // pass passport for configuration

//I will use ejs instead of jade that was shown in class for templating.
app.set('view engine', 'ejs');

app.use(session({ secret: '12345678' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session


//routers:
var authenticationRoutes = require('./routes/authenticationRoutes');
var indexRouter = require('./routes/indexRouter');
var packagesRoutes = require('./routes/packagesRoutes');
var profileRoutes = require('./routes/profileRoute');
var usersRoutes  = require('./routes/usersRoutes');
var vehicleReportsRoutes = require('./routes/vehicleReportsRoutes');
var vehicleWorkDaysRoutes = require('./routes/vehicleWorkDaysRoutes');

app.use('/', authenticationRoutes);
app.use('/', indexRouter);
app.use('/api', packagesRoutes);
app.use('/', profileRoutes);
app.use('/api', usersRoutes);
app.use('/api', vehicleWorkDaysRoutes);
app.use('/api', vehicleReportsRoutes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

//init MongoDB seed
require("./initDB.js")();


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json(err);
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  logger(err.message);
  res.json(err);
});


app.listen(port);

module.exports = app;
