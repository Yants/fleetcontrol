'use strict';
// grab the things we need
var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var User = require('./Users').schema;
var Packages = require('./Packages').schema;

var vehicleDaySchema = new Schema({
    vehicle_number : {type: String, index:true},
    packages:[{type: Schema.ObjectId,
    ref: 'Packages'}],
    crew: [User],
    status: String,
    updated_at: String,
    created_at : String,
});

vehicleDaySchema.pre('save', function(next){

    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    if (!this.status)
        this.status = "atWork";

    next();

});

var VehicleDay = mongoose.model('VehicleDays', vehicleDaySchema);

// make this available to our users in our Node applications
module.exports = VehicleDay;