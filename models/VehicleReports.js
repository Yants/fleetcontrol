'use strict';
// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// create a schema

var vehicleReportSchema = new Schema({
    vehicle_number : {type: String, index:true},
    speed: Number,
    updated_at: String,
    created_at : String,
    loc: { type: [Number], index:'2d'} // long, lat
}, { strict: false});

vehicleReportSchema.pre('save', function(next){

    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;
    //this.constructor.findOne().sort({created_at: -1}).exec(function(err, report) {
      //  if (!err && report){
        //    this.speed =
       //         getDistanceFromLatLonInKm(report.loc[0], report.loc[1]) * 360;
      //  }
       //  });

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    if (!this.status)
        this.status = "undelivered";

    next();

});

vehicleReportSchema.methods.getDistanceFromLatLonInKm = function (lon2,lat2) {
    var R = 6371; // Radius of the earth in km
    var lon1 = this.loc[0];
    var lat1 = this.loc[1];

    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}
var VehicleReport = mongoose.model('VehicleReports', vehicleReportSchema);

// make this available to our users in our Node applications
module.exports = VehicleReport;