'use strict';
// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
// create a schema

var packageSchema = new Schema({
    package_number : {type: String, index:true},
    from: String,
    to: String,
    status: String,
    updated_at: String,
    created_at : String,
    dest: { type: [Number], index:'2d'} // long, lat
}, { strict: false});

packageSchema.pre('save', function(next){

    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    if (!this.status)
        this.status = "undelivered";

    next();

});

var Package = mongoose.model('Packages', packageSchema);

// make this available to our users in our Node applications
module.exports = Package;