'use strinct';
// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt   = require('bcrypt-nodejs');
// create a schema

var userSchema = new Schema({
    name: String,
    userName: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    admin: Boolean,
    CivilID: String,
    Position: String,
    PhoneNumber: String,
    created_at: Date,
    updated_at: Date
});

// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.generateHashForOwnUser = function() {
    if (this.password){
        this.password =  bcrypt.hashSync(this.password, bcrypt.genSaltSync(8), null);

    }
    return this;
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};


userSchema.pre('save', function(next){

    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

    next();

});

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('Users', userSchema);

// make this available to our users in our Node applications
module.exports = User;