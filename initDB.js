
var mongoose = require('mongoose');
var User = require('./models/Users.js');
var Package = require('./models/Packages.js');
var VehicleWorkDay = require('./models/VehicleWorkDays.js');
var VehicleReport = require('./models/VehicleReports.js');
var async = require('async');
module.exports =  function () {
    var connection = mongoose.connect('mongodb://localhost/fleetControl', function () {
        successfulConnection(connection);
    });
};

function successfulConnection(connection) {
    var Schema = mongoose.Schema;
    mongoose.connection.db.dropDatabase();
        async.waterfall(
            [

                createUserSeed,
                createPackagesSeed,
                createVehicleWorkDaySeed,
                insertFirstReport,
                createVehicleReportSeed
            ], function(err, result) {
                console.log("DB init done.");
            });


}
var userCalled = false;
var createUserSeed = function (callback){
    new User({ name: "yan", userName:"admin", admin: true, password: "yan"}).generateHashForOwnUser().save(); // admin

    for (var i=0; i<10; i++){ //regular users
        var newUser = new User({ name : 'emp' + i, password: '12345', userName: "emp" + i});
        newUser.generateHashForOwnUser();
        newUser.save(function (err) {
            if (i==10 &&!userCalled){
                userCalled = true;
                callback();
            }
        });
    }
}

var packageCalled = false;
var createPackagesSeed = function (callback){
    for (var i=0; i<10; i++){
        var arr = (Math.floor(Math.random() * (999999999999 - 100000000000) + 100000000000));
        var str = String(arr);
        var newPackage = new Package({ package_number : 'Z' + str, from: 'yossi' +i, to: "shimon" + i,
        dest : [10.00011, 10.23232]});
        newPackage.save(function (err) {
            if (i==10 && !packageCalled){
                packageCalled = true;
                callback();
            }
        });
    }

}

var vehiCalled = false;
var createVehicleWorkDaySeed = function (callback) {
    var newVehicleWorkDay = new VehicleWorkDay();
    newVehicleWorkDay.vehicle_number = "12345";
    User.findOne({name: 'yan'}, function(err, user){
        //newVehicleWorkDay.crew = [{employee: user._id }];
        Package.findOne({status: "undelivered"}, function(err, package){
            newVehicleWorkDay.packages = [];
            newVehicleWorkDay.crew = [];
            newVehicleWorkDay.save(function (err) {
                if(!err) {
                    //findOneAndUpdate
                    newVehicleWorkDay.packages.push(package._id);
                    newVehicleWorkDay.crew.push(user);
                    newVehicleWorkDay.markModified('packages');
                    newVehicleWorkDay.markModified('crew');
                    newVehicleWorkDay.save(function (err) {
                        if (!vehiCalled) {
                            vehiCalled = true;
                            callback();
                        }
                    });
                }

            });
        });
    });
}

var insertFirstReport = function(callback){
    var newReport = new VehicleReport();
    newReport.vehicle_number = "12345";
    newReport.loc = [32.323854, 34.867194];

    newReport.save(function (err) {
        if(!err){
            callback();
        }
    })

}
var reportCalled = false;
var createVehicleReportSeed = function (callback){
    // https://www.google.com/maps/dir/Netanya,+Israel/Tel+Aviv-Yafo,+Israel/@32.2028771,34.7553485,12z/data=!3m1!4b1!4m13!4m12!1m5!1m1!1s0x151d400493c075d5:0x2cd995be543c3f22!2m2!1d34.853196!2d32.321458!1m5!1m1!1s0x151d4ca6193b7c1f:0xc1fb72a2c0963f90!2m2!1d34.7817676!2d32.0852999?hl=en

    var demo = [
        {lon:32.323533, lat:34.867111},
        {lon:32.323229, lat:34.867052},
        {lon:32.322987, lat:34.867006},
        {lon:32.322844, lat:34.865961}
    ];
    for (var i =0; i<demo.length; i++){

        (function (demo, i) {

            var newReport = new VehicleReport();

            newReport.loc = [demo[i].lon, demo[i].lat];
            newReport.vehicle_number = "12345";
            var waitTill = new Date(new Date().getTime() + 1000);
            while(waitTill > new Date()){}
            newReport.created_at = waitTill;
            newReport.save();
            VehicleReport.findOne({}, {}, { sort: { 'created_at' : -1 } }, function(err, report) {
                if (report) newReport.speed = newReport.getDistanceFromLatLonInKm(report.loc[0], report.loc[1]) * 360;

                newReport.save(function (err) {
                        if (i== demo.length && !reportCalled){
                            reportCalled = true;
                            callback();

                        }
                    });
            });
        })(demo,i);

    }
}

